jQuery(document).ready(function($){

    $('.view-intro .view-content').addClass('owl-carousel owl-theme');

    $('.view-intro .owl-carousel').owlCarousel({
        items: 1,
        loop:true,
        margin:0,
        nav:true,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
    }) 

});
