var gulp = require('gulp');
var sass = require('gulp-sass');

// sass
gulp.task('sass', function() {

    return gulp.src('./sass/*.scss')

        .pipe(sass({
            compress: false
        }))
        .pipe(gulp.dest('./css'))
  
});

gulp.task('default', gulp.series('sass', function(){
   gulp.watch( './sass/**/*.scss', gulp.series('sass'));
}));
